package com.eventapp.entities;

public class TicketCancelling {
	private int eId;
	private int totalNoOfTickets;

	public int geteId() {
		return eId;
	}

	public void seteId(int eId) {
		this.eId = eId;
	}

	public int getTotalNoOfTickets() {
		return totalNoOfTickets;
	}

	public void setTotalNoOfTickets(int totalNoOfTickets) {
		this.totalNoOfTickets = totalNoOfTickets;
	}

	public TicketCancelling() {
	}

	public TicketCancelling(int eId, int totalNoOfTickets) {
		super();
		this.eId = eId;
		this.totalNoOfTickets = totalNoOfTickets;
	}

}
