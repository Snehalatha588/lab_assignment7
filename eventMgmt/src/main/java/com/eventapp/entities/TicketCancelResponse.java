package com.eventapp.entities;

public class TicketCancelResponse {
	private String message;
	private double amountRefund;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public double getAmountRefund() {
		return amountRefund;
	}

	public void setAmountRefund(double amountRefund) {
		this.amountRefund = amountRefund;
	}

	public TicketCancelResponse() {
	}

	public TicketCancelResponse(String message, double amountRefund) {
		super();
		this.message = message;
		this.amountRefund = amountRefund;
	}

}
