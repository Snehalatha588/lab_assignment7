package com.eventapp.exceptions;

public class ResourceNotFoundException extends RuntimeException {
	private static final long serialVersionUID = 9121519334498250845L;

	public ResourceNotFoundException(String message) {
		super(message);
	}

}
